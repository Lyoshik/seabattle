package com.example;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Scan extends Field{
    Scanner scanner = new Scanner(System.in);
    public void doShot(){
        try {
            System.out.println("Введи координату X:");
            int x = scanner.nextInt();
            System.out.println("Введи координату Y:");
            int y = scanner.nextInt();
            try {
                FIELD[x][y] += 2;
                if (FIELD[x][y] == 2) {
                    System.out.println("МИМО!");
                }
                if (FIELD[x][y] == 3) {
                    System.out.println("УБИЛ!");
                }
                new PressAgainException();
                PressAgainException.exc(FIELD[x][y]);
            }
            catch (DontShootFieldTwiceException e) {
                System.out.println("Внимание!  " +e+ "  В одно и то же поле можно стрелять лишь раз!");
                FIELD[x][y] -= 2;
            }
        }
        catch (ArrayIndexOutOfBoundsException | InputMismatchException e){
            System.out.println("Внимание!  " +e+ "  Неверные координаты поля!");
        }
    }
    public void newField(){
        doShot();
        drawField();
    }
}


