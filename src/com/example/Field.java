package com.example;

import java.util.Random;


public class Field {
    public static final int SIZE = 10;
    public static final int[][] FIELD = new int[SIZE][SIZE];
    Random r = new Random();

    public void setField(){
        for (int i = 0; i < FIELD.length; i++) {
            for (int j = 0; j < FIELD[i].length; j++) {
                FIELD[i][j] = r.nextInt(2);
            }
        }
    }
    public void drawField(){
        for (int i = 0; i < FIELD.length; i++) {
            for (int j = 0; j < FIELD[i].length; j++) {
                System.out.print("  " + FIELD[i][j]);
            }
            System.out.println();
        }
    }

}
