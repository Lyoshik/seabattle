package com.example;

public class PressAgainException {
    public static void exc(int a) throws DontShootFieldTwiceException{
        if (a>=4){
            throw new DontShootFieldTwiceException(a);
        }
    }
}
